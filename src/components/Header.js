import fleur from '../assets/images/fleur.svg'

import React from 'react'

class Header extends React.Component {
  render() {
    return (
      <section id="header">
        <div className="inner">
          <span className="icon major">
            <span className="image fit">
              <img src={fleur} alt="" />
            </span>
          </span>
          <h1>
            We are <strong>Nola Kitten Foster</strong>
          </h1>
          <h2>a New Orleans based foster nursery for young kittens.</h2>
          <ul className="icons">
            <li>
              <a
                href="https://www.instagram.com/nolakittenfoster/"
                className="icon alt fa-instagram"
              >
                <span className="label">Instagram</span>
              </a>
            </li>
            <li>
              <a
                href="https://www.facebook.com/nolakittenfoster"
                className="icon alt fa-facebook"
              >
                <span className="label">Facebook</span>
              </a>
            </li>
            <li>
              <a
                href="https://twitter.com/nolakittenfstr"
                className="icon alt fa-twitter"
              >
                <span className="label">Twitter</span>
              </a>
            </li>
            <li>
              <a
                href="mailto:kittens@nolakittenfoster.org"
                className="icon alt fa-envelope"
              >
                <span className="label">Email</span>
              </a>
            </li>
          </ul>
          <ul className="actions">
            <li>
              <a href="#kittens" className="button scrolly">
                See our Kittens
              </a>
            </li>
          </ul>
        </div>
      </section>
    )
  }
}

export default Header

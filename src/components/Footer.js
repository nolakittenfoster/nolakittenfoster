import React from 'react'

class Footer extends React.Component {
  render() {
    return (
      <section id="footer">
        <ul className="copyright">
          <li>&copy; 2020 Nola Kitten Foster</li>
          <li>
            Design: <a href="http://html5up.net">HTML5 UP</a>
          </li>
        </ul>
      </section>
    )
  }
}

export default Footer

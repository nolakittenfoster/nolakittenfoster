import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'

class Homepage extends React.Component {
  componentDidMount() {
    let curatorScript = document.createElement('script')
    curatorScript.async = 1
    curatorScript.src =
      'https://cdn.curator.io/published/7b3b56c5-6e60-473a-a9e2-1184ec211d6d.js'
    let scriptElement = document.getElementsByTagName('script')[0]
    scriptElement.parentNode.insertBefore(curatorScript, scriptElement)

    let style = document.createElement('style');
    style.innerHTML = 'button.crt-load-more { padding: 0px; }'
    document.body.appendChild(style);
  }

  render() {
    const siteTitle = 'Nola Kitten Foster'

    return (
      <Layout>
        <Helmet title={siteTitle} />

        <section id="adopt" className="main style2">
          <div className="grid-wrapper">
            <div className="col-3"></div>
            <div className="col-6">
              <header className="major">
                <h2>Adopting a Cuddly Purr Machine</h2>
              </header>
              <p>
                We work with local animal shelters, including{' '}
                <a href="https://www.bigskyranch.org/catnip-foundation">
                  Big Sky Ranch / CATNIP Foundation
                </a>
                ,{' '}
                <a href="https://animalrescueneworleans.org/">
                  Animal Rescue New Orleans
                </a>
                &nbsp;and the{' '}
                <a href="https://www.la-spca.org/">Louisiana SPCA</a>. Adoption
                requires filling out an application with the right shelter.
              </p>
              <p>
                {' '}
                Contact us if you are interested in adopting one of our babies
                and we will put you in contact with the right people.
              </p>
            </div>
            <div className="col-3"></div>
          </div>
        </section>

        <section id="fostering" className="main style1 special">
          <div className="grid-wrapper">
            <div className="col-6">
              <header className="major">
                <h2>Kitten Fostering</h2>
              </header>
              <p>
                Young, and especially orphaned, kittens are one of the highest
                risk groups for being euthanized in municipal shelters. During
                the first couple months of life they have weak immune systems
                and require a level of hands-on care that surpasses the
                resources of many animal shelters. We help the shelters by
                providing a temporary home and loving care for the adorable
                little babies until they&apos;re big enough to be de-sexed,
                vaccinated, and placed for adoption.
              </p>
              <p>
                For more information we <strong>highly</strong> recommend{' '}
                <a href="https://orphankittenclub.org/">Orphan Kitten Club</a>
                &nbsp; which was put together by the incredible{' '}
                <a href="http://www.kittenlady.org/">
                  Hannah Shaw, aka Kitten Lady
                </a>
                , and her fantastic book{' '}
                <a href="http://www.kittenlady.org/tiny-but-mighty">
                  Tiny But Mighty
                </a>
                .
              </p>
            </div>
            <div className="col-6"></div>
          </div>
        </section>

        <section id="trap-neuter-return" className="main style2 special">
          <div className="grid-wrapper">
            <div className="col-6"></div>
            <div className="col-6">
              <header className="major">
                <h2>Trap-Neuter-Return</h2>
              </header>
              <p>
                Every kitten we foster started from two unsterilized cats.
                Female cats are fertile by six months of age and can have
                several litters of one to twelve kittens each per year! Left
                unchecked, this can lead to out of control cat populations. With
                Trap-Neuter-Return, we work in the community to trap community
                cats, take them to be vaccinated and de-sexed, and then return
                them to the colony they came from to continue living out their
                lives without the stress of rampant population growth.
              </p>
              <p>
                Please contact us if you see kittens living outdoors, or groups
                of cats without tipped ears. (When community cats have gone
                through TNR, their ears will be tipped to prevent trapping for
                TNR in the future.)
              </p>
              <p>
                For more information, see{' '}
                <a href="https://www.alleycat.org/resources/why-trap-neuter-return-feral-cats-the-case-for-tnr/">
                  Alley Cat: The Case for TNR
                </a>
                ,{' '}
                <a href="https://www.la-spca.org/feralcats">
                  LSPCA: Feral Cat Program
                </a>
                ,{' '}
                <a href="https://www.facebook.com/trapdatcat/">Trap Dat Cat</a>
                &nbsp; or{' '}
                <a href="https://orphankittenclub.org/fullcircle/">
                  Orphan Kitten Club: Full Circle TNR Program
                </a>
                .
              </p>
            </div>
          </div>
        </section>

        <section id="kittens" className="main style1">
          <div className="grid-wrapper">
            <div className="col-12">
              <div id="curator-feed-default-feed-layout">
                <a
                  href="https://curator.io"
                  target="_blank"
                  rel="noreferrer"
                  className="crt-logo crt-tag"
                >
                  Powered by Curator.io
                </a>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}

export default Homepage

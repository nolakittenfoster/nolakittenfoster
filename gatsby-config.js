module.exports = {
  siteMetadata: {
    title: 'Nola Kitten Foster',
    author: 'Monty Taylor',
    description: 'Just what it sounds like, Kitten Fostering in New Orleans',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Nola Kitten Foster',
        short_name: 'nolakittenfoster',
        start_url: '/',
        background_color: '#663399',
        theme_color: '#663399',
        display: 'minimal-ui',
        icon: 'src/assets/images/website-icon.png',
      },
    },
    'gatsby-plugin-sass',
  ],
}
